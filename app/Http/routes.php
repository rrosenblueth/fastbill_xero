<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//
//
//Route::get('/', function () {
//    return view('test.index');
//});

//Route::get('articles', function () {
//    echo "Esta es la seccion de articulos.";
//});

//Route::get('articles/{nombre?}', function ($nombre = "No coloco nombre") {
//    echo "El nombre que has colocado es: ".$nombre;
//});

////para definir la ruta y el controller
// Route::get('articles', [
//         'as' => 'articles',
//         'uses' => 'UserController@view'
//     ]
//);

////grupos de rutas
//Route::group(['prefix' => 'articles'], function(){
//    Route::get('view/{article?}', function($article = "Vacio"){
//        echo $article;
//    });
//});
//
////para crear un controlador y asignarlo.
//Route::group(['prefix' => 'articles'], function(){
//    Route::get('view/{id}', [
//        'uses' => 'LoginRegister@view',
//        'as' => 'articlesView'
//    ]);
//});
//
//Route::group(['prefix' => 'articles'], function(){
//    Route::get('view/{id}', [
//        'uses' => 'LoginRegister@view',
//        'as' => 'articlesView'
//    ]);
//});
//
//Route::get('/register', function () {
//    return view('register');
//});
//


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//TEST
Route::get('/test', 'TestController@index');


//REGISTER WEBHOOKS
Route::get('/register', 'RegisterController@index');
Route::post('/register', 'RegisterController@registerWebhook');
Route::get('/register/obtener', 'RegisterController@retrieveWebhook');
Route::post('/register/delete', 'RegisterController@deleteWebhook');

//API
Route::group(['prefix' => 'api'], function () {

    Route::group(['prefix' => 'customer'], function () {
        Route::post('update', 'CustomerController@update');
        Route::post('create', 'CustomerController@create');
    });

    Route::group(['prefix' => 'invoice'], function () {
        Route::post('create', 'InvoiceController@create');
        Route::post('update', 'InvoiceController@update');
    });

    Route::group(['prefix' => 'payment'], function () {
        Route::get('create', 'PaymentController@create');
        Route::get('update', 'PaymentController@update');
    });
});


