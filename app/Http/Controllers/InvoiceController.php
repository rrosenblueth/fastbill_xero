<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Helpers\Contracts\PrivateAppHandlerContract;

class InvoiceController extends Controller
{
    public function __construct(PrivateAppHandlerContract $privateAppHandler)
    {
        $this->privateAppHandler = $privateAppHandler;
    }

    public function create(Request $request)
    {
        return $this->privateAppHandler->CreateUpdateInvoice($request);
    }

    public function update(Request $request)
    {
        return $this->privateAppHandler->CreateUpdateInvoice($request);
    }
}
