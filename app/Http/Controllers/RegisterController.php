<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Contracts\RegisterWebhookContract;

class RegisterController extends Controller
{
    public function __construct(RegisterWebhookContract $registerWebhook)
    {
        $this->registerWebhook = $registerWebhook;
    }

    public function index()
    {
        return view('register');
    }

    public function registerWebhook(Request $request)
    {
        $message = $this->registerWebhook->register($request->input('fastBillEmail'),$request->input('fastBillAPIKey'),$request->input('endpoint'),$request->input('event'));
        return view('register',['message' => $message]);
    }

    public function retrieveWebhook(Request $request)
    {
        $webhooks = $this->registerWebhook->getAll($request->input('fastBillEmail'),$request->input('fastBillAPIKey'));
        return view('register',['webhook' => $webhooks]);
    }

    public function deleteWebhook(Request $request)
    {
        $deleteMessage = $this->registerWebhook->delete($request->input('fastBillEmail'),$request->input('fastBillAPIKey'),$request->input('webhookId'));
        return view('register',['deleteMessage' => $deleteMessage]);
    }
}
