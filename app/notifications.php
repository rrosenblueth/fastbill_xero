<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notifications extends Model
{
    protected $table = 'notifications';
    protected $primaryKey = 'id';
    //protected $fillable = array('id','notification_id','notification_type','created_at', 'updated_at');
    protected $fillable = array('id','notification_id','notification_type','fastbill_id','xero_id','created_at', 'updated_at','notification_action');
}
