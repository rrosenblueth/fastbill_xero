<?php
/**
 * Created by PhpStorm.
 * User: rosenblueth
 * Date: 12/02/16
 * Time: 18:17
 */

namespace app\Helpers;

use App\Helpers\Contracts\RegisterWebhookContract;

class RegisterWebhook implements RegisterWebhookContract
{
    public function register($email, $apiKey,$endpoint,$event)
    {
        //SET DATA FOR FASTBILL
        $url = "https://my.fastbill.com/api/1.0/api.php";
        //$eventsWebhooks = "customer.created,customer.updated,invoice.created,invoice.completed,estimate.created,estimate.updated";
        //$eventsWebhooks = "customer.created";
        $eventsWebhooks = $event;

        //GET
        $webhookCheckData = array("SERVICE" => "webhook.get");

        $dataJson = json_encode($webhookCheckData);

        $headers = array(
            'Content-Type:application/xml',
            'Authorization: Basic ' . base64_encode($email.':'.$apiKey)//'rod_12190@hotmail.com:235a1577331b909950253a73d8847e1bm59x8mCTwLQMrRXs5XGiS32Tnq0oPvub') // <---
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);

        $obj = json_decode($server_output);

        //check if there is any response. When the webhook is not registered, then there is no RESPONSE variable in the array.
        if (array_key_exists('RESPONSE', $obj) && isset($obj->RESPONSE->WEBHOOKS)) {
            //only one webook would be registered for now.
            if($obj->RESPONSE->WEBHOOKS[0]->EVENTS == $eventsWebhooks)
            {
                return "Webhook already exists.";

            }else
            {
                //return "Webhook doesn't exist.";


                //$dataRequest = array("TYPE" => "url", "ENDPOINT" => "http://prueba.laravel.com/public/index.php/api/customer/create", "EVENTS" => $eventsWebhooks);
                $dataRequest = array("TYPE" => "url", "ENDPOINT" => $endpoint, "EVENTS" => $eventsWebhooks);


                //    FILL THE DATA
                //CREATE
                $webhookData = array(
                    "SERVICE" => "webhook.create"
                ,"DATA" => $dataRequest
                );

                //CUSTOMER CREATE DATA AS JSON
                $dataJson = json_encode($webhookData);

                //HEADERS - Authorization
                $headers = array(
                    'Content-Type:application/xml',
                    'Authorization: Basic ' . base64_encode($email.':'.$apiKey)//'rod_12190@hotmail.com:235a1577331b909950253a73d8847e1bm59x8mCTwLQMrRXs5XGiS32Tnq0oPvub') // <---
                );

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  //REMOVE CERTIFICATE

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJson);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $server_output = curl_exec($ch);

                curl_close($ch);

//                echo "<BR>";
//                echo $server_output;
//                echo "<BR>";
//                return "webhook registrado";

                return $server_output;

            }
        }else{
            $dataRequest = array("TYPE" => "url", "ENDPOINT" => "http://prueba.laravel.com/public/index.php/api/customer/create", "EVENTS" => $eventsWebhooks);

            //    FILL THE DATA
            //CREATE
            $webhookData = array(
                "SERVICE" => "webhook.create"
            ,"DATA" => $dataRequest
            );

            //CUSTOMER CREATE DATA AS JSON
            $dataJson = json_encode($webhookData);

            //HEADERS - Authorization
            $headers = array(
                'Content-Type:application/xml',
                'Authorization: Basic ' . base64_encode($email.':'.$apiKey)//'rod_12190@hotmail.com:235a1577331b909950253a73d8847e1bm59x8mCTwLQMrRXs5XGiS32Tnq0oPvub') // <---
            );

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  //REMOVE CERTIFICATE

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJson);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec($ch);

            curl_close($ch);

//            echo "<BR>";
//            echo $server_output;
//            echo "<BR>";
            //return "webhook registrado";
            return $server_output;
        }
    }

    public function getAll($email, $apiKey)
    {

        //SET DATA FOR FASTBILL
        $url = "https://my.fastbill.com/api/1.0/api.php";

        //GET
        $webhookCheckData = array("SERVICE" => "webhook.get");
        $dataJson = json_encode($webhookCheckData);

        $headers = array(
            'Content-Type:application/xml',
            'Authorization: Basic ' . base64_encode($email.':'.$apiKey)//'rod_12190@hotmail.com:235a1577331b909950253a73d8847e1bm59x8mCTwLQMrRXs5XGiS32Tnq0oPvub') // <---
            //'Authorization: Basic ' . base64_encode('rod_12190@hotmail.com:235a1577331b909950253a73d8847e1bm59x8mCTwLQMrRXs5XGiS32Tnq0oPvub')//'rod_12190@hotmail.com:235a1577331b909950253a73d8847e1bm59x8mCTwLQMrRXs5XGiS32Tnq0oPvub') // <---
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);

        //$obj = json_decode($server_output);

        return $server_output;
    }

    public function delete($email, $apiKey, $webhookID)
    {
        //SET DATA FOR FASTBILL
        $url = "https://my.fastbill.com/api/1.0/api.php";

        //DELETE
        $dataDelete = array("WEBHOOK_ID" => $webhookID);
        $webhookData = array(
            "SERVICE" => "webhook.delete"
            ,"DATA" => $dataDelete
        );

        $dataJson = json_encode($webhookData);

        $headers = array(
            'Content-Type:application/xml',
            'Authorization: Basic ' . base64_encode($email.':'.$apiKey)//'rod_12190@hotmail.com:235a1577331b909950253a73d8847e1bm59x8mCTwLQMrRXs5XGiS32Tnq0oPvub') // <---
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);

        //$obj = json_decode($server_output);

        return $server_output;
    }
}