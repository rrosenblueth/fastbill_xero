<?php
/**
 * Created by PhpStorm.
 * User: rosenblueth
 * Date: 16/02/16
 * Time: 16:39
 */

namespace app\Helpers;

use App\Helpers\Contracts\PrivateAppHandlerContract;
use XeroOAuth;
use App;

class PrivateAppHandler implements PrivateAppHandlerContract
{
    public function __construct( )
    {
        //PARAMETERS
        $this->signatures = array(
            'consumer_key' => 'KN4BXP9RSHY24ZEP3WSE3E2TV3LAP4',
            'shared_secret' => 'LITJ6GZ7TLD3T4U4UCQLUXQNNZNGVU',
            // API versions
            'core_version' => '2.0',
            'payroll_version' => '1.0',
            'file_version' => '1.0',
            // certs
            'rsa_private_key' => __DIR__ . '/certs/privatekey2.pem',
            'rsa_public_key' => __DIR__ . '/certs/publickey2.cer'
        );

        $this->xeroOAuthData = array(
            'application_type' => "Private",
            'oauth_callback' => "oob",
            'user_agent' => "XeroOAuth-PHP Private App Test - Pruebita"
        );

        $this->XeroOauth = new XeroOAuth(array_merge($this->signatures, $this->xeroOAuthData));
    }


    public function CreateUpdateCustomer($request)
    {
        //CHECK IF ALL DATA IS CORRECT
        $initialCheck = $this->XeroOauth->diagnostics();

        $checkErrors = count($initialCheck);
        if ($checkErrors > 0) {
            $log = "";
            foreach ($initialCheck as $check) {
                $log = '        '.$log. '  Error: ' . $check . PHP_EOL; //TODO: write error log - perhaps save in database.
            }
            return $log;
        } else {
            //CONTINUE WITH THE REQUEST
            $this->XeroOauth->config['access_token'] = $this->XeroOauth->config ['consumer_key'];
            $this->XeroOauth->config['access_token_secret'] = $this->XeroOauth->config ['shared_secret'];
            $this->XeroOauth->config['session_handle'] = '';

            $id = $request->input('id');
            $type = $request->input('type');

            $customer = $request->input('customer');
            $dateCreated = $request->input('created');
            //$invoice = $request->input('invoice');

//            Example of minimum elements required to add a new contact
//            <Contact>
//                <Name>ABC Limited</Name>
//            </Contact>

            //RESPONSE
            //{"id":4014,
            //"type":"customer.created",
            //"customer":{  "customer_id":"2010266","customer_number":"87","days_for_payment":"14","payment_type":"ueberweisung","bank_name":"",
            //      "bank_account_owner":"","bank_account_number":"","bank_code":"","bank_iban":"","bank_bic":"","bank_account_mandate_reference":"",
            //      "show_payment_notice":"1","customer_type":"business","organization":"compania6","title":"","salutation":"","first_name":"compania6",
            //      "last_name":"compania6","address":"","address_2":"","zipcode":"","city":"","country_code":"BO","vat_id":"","email":"compania6@compania.com",
            //      "phone":"","fax":"","website":""},
            //"created":"2016-02-25 18:19:30"}


            //SEARCH IN DATABASE, IF THERE IS ONE REGSITERED, THEN USE THIS TO UPDATE
            //                      <ContactID>81ef6f40-8aaa-4975-b823-33e028b5127f</ContactID>  //TO UPDATE

            //$customerExists = App\notifications::where('fastbill_id', '=', 123123123134)->get();
            //return $customerExists;

//            $customerExistsXero = 0;
//            if (  !is_null($customerXero = App\notifications::where('fastbill_id', '=',$customer['customer_id'])->first())) //return $model;
//                $customerExistsXero = $customerXero;

            //return $model->xero_id;

            $xml = "<Contacts>";
            $xeroid = false;
            if (  !is_null($customerXero = App\notifications::where('fastbill_id', '=',$customer['customer_id'])->where('notification_type', '=','customer')->first())) //return $model;
            {
                $xml = $xml."<ContactID>".$customerXero->xero_id."</ContactID>";
                $xeroid = true;
            }
            $xml = $xml."
                     <Contact>
                       <ContactNumber>".$customer['customer_number']."</ContactNumber>
                       <Name>". $customer['organization']."</Name>
                       <EmailAddress> ".$customer['email']."</EmailAddress>
                       <FirstName>" . $customer['first_name']."</FirstName>
                       <LastName>".$customer['last_name']."</LastName>
                       <Addresses>
                        <Address>
                          <AddressType>STREET</AddressType>
                            <AddressLine1>".$customer['address']."</AddressLine1>
                            <AddressLine2>".$customer['address_2']."</AddressLine2>
                            <City>".$customer['city']. "</City>
                            <PostalCode>".$customer['zipcode']."</PostalCode>
                            <Country>".$customer['country_code']."</Country>
                          </Address>
                        </Addresses>
                        <Phones>
                          <Phone>
                            <PhoneType>DEFAULT</PhoneType>
                            <PhoneNumber>".$customer['phone']."</PhoneNumber>
                          </Phone>
                          <Phone>
                            <PhoneType>FAX</PhoneType>
                            <PhoneNumber>".$customer['fax']."</PhoneNumber>
                          </Phone>
                        </Phones>
                     </Contact>
                   </Contacts>";

            $response = $this->XeroOauth->request('POST', $this->XeroOauth->url('Contacts', 'core'), array(), $xml);
            if ($this->XeroOauth->response['code'] == 200) {
                $contact = $this->XeroOauth->parseResponse($this->XeroOauth->response['response'], $this->XeroOauth->response['format']);
                //return $this->XeroOauth->response['response'];   //devuelve todo

//                <Response xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
//    <Id>e3fb8172-76f4-42ae-933d-6a062b41c7b2</Id>
//    <Status>OK</Status>
//    <ProviderName>FB4</ProviderName>
//    <DateTimeUTC>2016-03-29T14:09:18.2129593Z</DateTimeUTC>
//    <Contacts>
//        <Contact>
//            <ContactID>e9c9c0d3-93b0-45ed-9dc9-7625d1f484a2</ContactID>
//            <ContactNumber>182</ContactNumber>
//            <ContactStatus>ACTIVE</ContactStatus>
//            <Name>customer del invoice1</Name>
//            <FirstName>customer del invoice1</FirstName>
//            <EmailAddress>juanperez@lospolloshermanos.com</EmailAddress>
//            <Addresses>
//                <Address>
//                    <AddressType>POBOX</AddressType>
//
//                </Address>
//                <Address>
//                    <AddressType>STREET</AddressType>
//                    <AddressLine1>Berlinerstr. 23</AddressLine1>
//                    <City>Frankfurt</City>
//                    <PostalCode>12345</PostalCode>
//                    <Country>DE</Country>
//
//                </Address>
//
//            </Addresses>
//            <Phones>
//                <Phone>
//                    <PhoneType>DEFAULT</PhoneType>
//                    <PhoneNumber>1232414</PhoneNumber>
//
//                </Phone>
//                <Phone>
//                    <PhoneType>MOBILE</PhoneType>
//
//                </Phone>
//                <Phone>
//                    <PhoneType>DDI</PhoneType>
//
//                </Phone>
//                <Phone>
//                    <PhoneType>FAX</PhoneType>
//
//                </Phone>
//
//            </Phones>
//            <UpdatedDateUTC>2016-03-29T13:47:47.733</UpdatedDateUTC>
//            <IsSupplier>false</IsSupplier>
//            <IsCustomer>false</IsCustomer>
//
//        </Contact>
//
//    </Contacts>
//
//</Response>

                //return $this->XeroOauth->response['format'];
                //return $request->input('id');

                    $notification = new \App\notifications();
                    $notification->notification_id = $request->input('id');
                    $notification->notification_type = 'customer';
                    $notification->fastbill_id = $customer['customer_id'];
                    $notification->xero_id = $contact->Contacts[0]->Contact->ContactID;
                    if(!$xeroid)
                    {
                        $notification->notification_action = "created";
                    }else{
                        $notification->notification_action = "updated";
                    }

//                    if ( $xeroid) //return $model;
//                    {
//                        $notification->created_at = $customerXero->created_at;
//                    }
                    $notification->save();

//                    App\Flight::where('notification_type', 'customer')
//                        ->where('fastbill_id', $customer['customer_id'])
//                        ->update(['xero_id' => $contact->Contacts[0]->Contact->ContactID]);

                return "" . count($contact->Contacts[0]) . " contact created/updated in this Xero organisation.";
            } else {
                //outputError($XeroOAuth);
                return ' Mega Error: ' . $this->XeroOauth->response['response'] . PHP_EOL;
                //TODO: write error log - perhaps save in database.
            }
        }
    }

    public function CreateUpdateInvoice($request)
    {
        //CHECK IF ALL DATA IS CORRECT
        $initialCheck = $this->XeroOauth->diagnostics();

        $checkErrors = count($initialCheck);
        if ($checkErrors > 0) {
            $log = "";
            foreach ($initialCheck as $check) {
                $log = '        '.$log. '  Error: ' . $check . PHP_EOL; //TODO: write error log - perhaps save in database.
            }
            return $log;
        } else {
            //CONTINUE WITH THE REQUEST
            $this->XeroOauth->config['access_token'] = $this->XeroOauth->config ['consumer_key'];
            $this->XeroOauth->config['access_token_secret'] = $this->XeroOauth->config ['shared_secret'];
            $this->XeroOauth->config['session_handle'] = '';

            $id = $request->input('id');
            $type = $request->input('type');

            $customer = $request->input('customer');
            $dateCreated = $request->input('created');
            $invoiceFB = $request->input('invoice');

//            Example of minimum elements required to add a new contact
//            <Contact>
//                <Name>ABC Limited</Name>
//            </Contact>

            //RESPONSE
            //{"id":4014,
            //"type":"customer.created",
            //"customer":{  "customer_id":"2010266","customer_number":"87","days_for_payment":"14","payment_type":"ueberweisung","bank_name":"",
            //      "bank_account_owner":"","bank_account_number":"","bank_code":"","bank_iban":"","bank_bic":"","bank_account_mandate_reference":"",
            //      "show_payment_notice":"1","customer_type":"business","organization":"compania6","title":"","salutation":"","first_name":"compania6",
            //      "last_name":"compania6","address":"","address_2":"","zipcode":"","city":"","country_code":"BO","vat_id":"","email":"compania6@compania.com",
            //      "phone":"","fax":"","website":""},
            //"created":"2016-02-25 18:19:30"}


            //SEARCH IN DATABASE, IF THERE IS ONE REGSITERED, THEN USE THIS TO UPDATE
            //                      <ContactID>81ef6f40-8aaa-4975-b823-33e028b5127f</ContactID>  //TO UPDATE

            $xml = "<Invoices>
                    <Invoice>
                        <Type>ACCREC</Type>";

            if (  !is_null($customerXero = App\notifications::where('fastbill_id', '=',$customer['customer_id'])->where('notification_type', '=','customer')->first())) //return $model;
            {
                $xml = $xml.
                    "<Contact>
                          <ContactID>".$customerXero->xero_id."</ContactID>
                    </Contact>";
            }
            $xml = $xml."
                        <Date>". $invoiceFB['invoice_date']."</Date>
                        <DueDate>". $invoiceFB['due_date']."</DueDate>
                        <LineAmountTypes>Exclusive</LineAmountTypes>
                        <LineItems>";

            foreach($invoiceFB['items'] as $lineitem) {
                $xml = $xml." <LineItem>
                            <Description>".$lineitem['description']."</Description>
                            <Quantity>".$lineitem['quantity']."</Quantity>
                            <UnitAmount>".$lineitem['unit_price']."</UnitAmount>
                          </LineItem>";
            }
                       $xml = $xml."</LineItems>";
                    if($invoiceFB['invoice_number'] != "")
                    {
                        $xml = $xml."<InvoiceNumber>". $invoiceFB['invoice_number']."</InvoiceNumber>";
                    }

                    $xml = $xml."<Url>". $invoiceFB['document_url']."</Url>
                       <CurrencyCode>". $invoiceFB['currency_code']."</CurrencyCode>";
            if($invoiceFB['type'] == "outgoing")
            {
                $xml = $xml."<Status>AUTHORISED</Status>";
            }
            if($invoiceFB['type'] == "draft")
            {
                $xml = $xml."<Status>DRAFT</Status>";
            }
            $xml = $xml."</Invoice> </Invoices>";

            $response = $this->XeroOauth->request('POST', $this->XeroOauth->url('Invoices', 'core'), array(), $xml);
            if ($this->XeroOauth->response['code'] == 200) {
                $invoiceXero = $this->XeroOauth->parseResponse($this->XeroOauth->response['response'], $this->XeroOauth->response['format']);

                $notification = new \App\notifications();
                $notification->notification_id = $request->input('id');
                $notification->notification_type = 'invoice';
                $notification->fastbill_id = $invoiceFB['invoice_id'];
                $notification->xero_id = $invoiceXero->Invoices[0]->Invoice->InvoiceID;

                if (  $invoiceFB['type'] == "draft")
                {
                    $notification->notification_action = "created";
                }else{
                    $notification->notification_action = "completed";
                }

                $notification->save();

                return "" . count($invoiceXero->Invoices[0]) . " invoice created/completed in this Xero organisation.";
            } else {
                //outputError($XeroOAuth);
                return 'Error: ' . $this->XeroOauth->response['response'] . PHP_EOL;
            }
        }
    }

//    public function UpdateCustomer($request){
//        //CHECK IF ALL DATA IS CORRECT
//        $initialCheck = $this->XeroOauth->diagnostics();
//
//        $checkErrors = count($initialCheck);
//        if ($checkErrors > 0) {
//            $log = "";
//            foreach ($initialCheck as $check) {
//                $log = '        '.$log. '  Error: ' . $check . PHP_EOL; //TODO: write error log - perhaps save in database.
//            }
//            return $log;
//        } else {
//            //CONTINUE WITH THE REQUEST
//            $this->XeroOauth->config['access_token'] = $this->XeroOauth->config ['consumer_key'];
//            $this->XeroOauth->config['access_token_secret'] = $this->XeroOauth->config ['shared_secret'];
//            $this->XeroOauth->config['session_handle'] = '';
//
//            $id = $request->input('id');
//            $type = $request->input('type');
//
//            $customer = $request->input('customer');
//            $dateCreated = $request->input('created');
//            //$invoice = $request->input('invoice');
//
////            Example of minimum elements required to add a new contact
////            <Contact>
////                <Name>ABC Limited</Name>
////            </Contact>
//
//            //RESPONSE
//            //{"id":4014,
//            //"type":"customer.created",
//            //"customer":{  "customer_id":"2010266","customer_number":"87","days_for_payment":"14","payment_type":"ueberweisung","bank_name":"",
//            //      "bank_account_owner":"","bank_account_number":"","bank_code":"","bank_iban":"","bank_bic":"","bank_account_mandate_reference":"",
//            //      "show_payment_notice":"1","customer_type":"business","organization":"compania6","title":"","salutation":"","first_name":"compania6",
//            //      "last_name":"compania6","address":"","address_2":"","zipcode":"","city":"","country_code":"BO","vat_id":"","email":"compania6@compania.com",
//            //      "phone":"","fax":"","website":""},
//            //"created":"2016-02-25 18:19:30"}
//
//
//            $xml = "<Contacts>
//                     <Contact>
//                       <ContactNumber>" . $customer['customer_number']. "</ContactNumber>
//                       <Name>" . $customer['organization']. "</Name>
//                       <EmailAddress>".$customer['email']."</EmailAddress>
//                       <FirstName>" . $customer['first_name']."</FirstName>
//                       <LastName>".$customer['last_name']. "</LastName>
//                       <Addresses>
//                        <Address>
//                          <AddressType>STREET</AddressType>
//                            <AddressLine1>".$customer['address']. "</AddressLine1>
//                            <AddressLine2>".$customer['address_2']. "</AddressLine2>
//                            <City>".$customer['city']. "</City>
//                            <PostalCode>".$customer['zipcode']. "</PostalCode>
//                            <Country>".$customer['country_code']. "</Country>
//                          </Address>
//                        </Addresses>
//                        <Phones>
//                          <Phone>
//                            <PhoneType>DEFAULT</PhoneType>
//                            <PhoneNumber>".$customer['phone']. "</PhoneNumber>
//                          </Phone>
//                          <Phone>
//                            <PhoneType>FAX</PhoneType>
//                            <PhoneNumber>".$customer['fax']. "</PhoneNumber>
//                          </Phone>
//                        </Phones>
//                     </Contact>
//                   </Contacts>";
//
//            $response = $this->XeroOauth->request('POST', $this->XeroOauth->url('Contacts', 'core'), array(), $xml);
//            if ($this->XeroOauth->response['code'] == 200) {
//                $contact = $this->XeroOauth->parseResponse($this->XeroOauth->response['response'], $this->XeroOauth->response['format']);
//                //TODO: when a customer is added, then his ID needs to be saved in a database to keep track of what has been saved.
//                return "" . count($contact->Contacts[0]) . " contact created/updated in this Xero organisation.";
//            } else {
//                //outputError($XeroOAuth);
//                return 'Error: ' . $this->XeroOauth->response['response'] . PHP_EOL;
//            }
//        }
//    }
}