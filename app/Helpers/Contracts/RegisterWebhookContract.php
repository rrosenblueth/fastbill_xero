<?php
/**
 * Created by PhpStorm.
 * User: rosenblueth
 * Date: 12/02/16
 * Time: 18:11
 */

namespace App\Helpers\Contracts;

Interface RegisterWebhookContract
{
    public function register($email, $apiKey, $endpoint, $event);

}