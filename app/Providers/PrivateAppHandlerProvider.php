<?php
/**
 * Created by PhpStorm.
 * User: rosenblueth
 * Date: 16/02/16
 * Time: 17:02
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\PrivateAppHandler;

class PrivateAppHandlerProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Helpers\Contracts\PrivateAppHandlerContract', function(){

            return new PrivateAppHandler();

        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['App\Helpers\Contracts\PrivateAppHandlerContract'];
    }

}