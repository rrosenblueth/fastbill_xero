@extends('admin.template.main')

@section('title','Register')

@section('content')


    {!! Form::open(array('url' => 'register', 'method' => 'POST')) !!}
        <h4> FASTBILL - XERO </h4>
        <h3> Please enter your FastBill credentials </h3>
    <br>
        {!! Form::label('fastBillEmail','Email',['style' => 'text-align: right,display: inline-block']) !!}
        {!! Form::email('fastBillEmail','rod_12190@hotmail.com',['placeholder' => 'Email','class' => 'form-control']) !!}

        {!! Form::label('fastBillAPIKey','API Key') !!}
        {!! Form::text('fastBillAPIKey','235a1577331b909950253a73d8847e1bm59x8mCTwLQMrRXs5XGiS32Tnq0oPvub',['class' => 'form-control','placeholder' => 'API Key']) !!}

        {!! Form::label('endpoint','Endpoint') !!}
        {!! Form::text('endpoint','http://prueba.laravel.com',['class' => 'form-control']) !!}

        {!! Form::label('event','Event Webhook') !!}
        {!! Form::text('event','customer.created,customer.updated,invoice.created,invoice.completed,estimate.created,estimate.updated',['class' => 'form-control']) !!}

        {!! Form::submit('Connect') !!}

    {!! Form::close() !!}
    <br><br>
    <div style="color: white;" >
        {{ isset($message) ? $message : ''}}
    </div>
    <br>



        {{--<div class="page-header">--}}
            {{--<h1>FASTBILL - XERO </h1>--}}
        {{--</div>--}}
        {{--{!! Form::open(array('url' => 'register', 'method' => 'POST')) !!}--}}
        {{--<div class="panel panel-info">--}}
            {{--<div class="panel-heading">--}}
                {{--<h4 >Please insert your FastBill Credentials.</h4>--}}
            {{--</div>--}}
            {{--<div class="panel-body">--}}
                {{--<div class="form-group">--}}
                    {{--{!! Form::label('fastBillEmail','Email') !!}--}}
                    {{--{!! Form::email('fastBillEmail','rod_12190@hotmail.com',['class' => 'form-control']) !!}--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--{!! Form::label('fastBillAPIKey','API Key') !!}--}}
                    {{--{!! Form::text('fastBillAPIKey','235a1577331b909950253a73d8847e1bm59x8mCTwLQMrRXs5XGiS32Tnq0oPvub',['class' => 'form-control']) !!}--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--{!! Form::label('endpoint','Endpoint') !!}--}}
                    {{--{!! Form::text('endpoint','http://prueba.laravel.com',['class' => 'form-control']) !!}--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--{!! Form::label('event','Event Webhook') !!}--}}
                    {{--{!! Form::text('event','customer.created,customer.updated,invoice.created,invoice.completed,estimate.created,estimate.updated',['class' => 'form-control']) !!}--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
            {{--{!! Form::submit('Connect',['class' => 'btn btn-primary']) !!}--}}
        {{--</div>--}}
        {{--{!! Form::close() !!}--}}
        {{--<br><br>--}}



    <div class="container">

        {{--<div class="page-header">--}}
            {{--<h1>FASTBILL - XERO </h1>--}}
        {{--</div>--}}
        {{--{!! Form::open(array('url' => 'register', 'method' => 'POST')) !!}--}}
        {{--<div class="panel panel-info">--}}
            {{--<div class="panel-heading">--}}
                {{--<h4 >Please insert your FastBill Credentials.</h4>--}}
            {{--</div>--}}
            {{--<div class="panel-body">--}}
                 {{--<div class="form-group">--}}
                    {{--{!! Form::label('fastBillEmail','Email') !!}--}}
                    {{--{!! Form::email('fastBillEmail','rod_12190@hotmail.com',['class' => 'form-control']) !!}--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--{!! Form::label('fastBillAPIKey','API Key') !!}--}}
                    {{--{!! Form::text('fastBillAPIKey','235a1577331b909950253a73d8847e1bm59x8mCTwLQMrRXs5XGiS32Tnq0oPvub',['class' => 'form-control']) !!}--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--{!! Form::label('endpoint','Endpoint') !!}--}}
                    {{--{!! Form::text('endpoint','http://prueba.laravel.com',['class' => 'form-control']) !!}--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--{!! Form::label('event','Event Webhook') !!}--}}
                    {{--{!! Form::text('event','customer.created,customer.updated,invoice.created,invoice.completed,estimate.created,estimate.updated',['class' => 'form-control']) !!}--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--{!! Form::submit('Connect',['class' => 'btn btn-primary']) !!}--}}
        {{--</div>--}}
        {{--{!! Form::close() !!}--}}
        {{--<br><br>--}}
        {{--<div {{ isset($message) ? 'class="alert alert-success" role="alert"' : ''}} >--}}
            {{--{{ isset($message) ? $message : ''}}--}}
        {{--</div>--}}
        {{--<br><br>--}}


        {!! Form::open(array('url' => 'register/obtener', 'method' => 'GET')) !!}
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4 >Get all Webhooks</h4>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    {!! Form::label('fastBillEmail','Email') !!}
                    {!! Form::email('fastBillEmail','rod_12190@hotmail.com',['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('fastBillAPIKey','API Key') !!}
                    {!! Form::text('fastBillAPIKey','235a1577331b909950253a73d8847e1bm59x8mCTwLQMrRXs5XGiS32Tnq0oPvub',['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            {!! Form::submit('Get Webhooks',['class' => 'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
        <br><br>
        <div style="color: white;" >
            {{ isset($webhook) ? $webhook : ''}}
       </div>
        <br><br>

        {!! Form::open(array('url' => 'register/delete', 'method' => 'POST')) !!}
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 >Delete Webhook</h4>
            </div>
            <div class="panel-body">

                <div class="form-group">
                    {!! Form::label('fastBillEmail','Email') !!}
                    {!! Form::email('fastBillEmail','rod_12190@hotmail.com',['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('fastBillAPIKey','API Key') !!}
                    {!! Form::text('fastBillAPIKey','235a1577331b909950253a73d8847e1bm59x8mCTwLQMrRXs5XGiS32Tnq0oPvub',['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('webhookId','Webhook ID') !!}
                    {!! Form::text('webhookId',null,['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            {!! Form::submit('Delete Webhook',['class' => 'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
        <br><br>
        <div style="color: white;" >
            {{ isset($deleteMessage) ? $deleteMessage : ''}}
        </div>
        <br><br>
    </div>
@endsection

