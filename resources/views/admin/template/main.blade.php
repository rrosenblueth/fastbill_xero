<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        @yield ('title','Default') | Data
    </title>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    {{--<link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>
    <section>
        @yield('content')
    </section>
    <!-- Javascript -->
    <script src="{{asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

</body>
</html>